package controller;

import java.util.LinkedList;

import model.MySQLDatabase;
import model.Rechteck;
import view.Rechteckgenerator;

public class BunteRechteckeController {

	private LinkedList<Rechteck> rechtecke;
	private MySQLDatabase db;
	
	public BunteRechteckeController(LinkedList<Rechteck> rechtecke) {
		super();
		this.rechtecke = rechtecke;
		this.db = new MySQLDatabase();
	}
	
	public BunteRechteckeController() {
		//this.rechtecke = new LinkedList<Rechteck>();
		this.db = new MySQLDatabase();
		rechtecke = this.db.getAlleRechtecke();
	}
	
	public void add(Rechteck rechteck) {
		this.rechtecke.add(rechteck);
		this.db.rechteckEintragen(rechteck);
		
	}
	
	public void reset() {
		this.rechtecke.clear();
	}
	
	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(LinkedList<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}
	
	@Override
	public String toString() {
		String ausgabe = "BunteRechteckeController [rechtecke=";
		for(int i = 0;this.rechtecke.size()>i;i++) {
			ausgabe += " [" + this.rechtecke.get(i).toString();
			if (i != this.rechtecke.size()-1) {
				ausgabe += ",";
			}
		}
		ausgabe += "]";
		return ausgabe;
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		rechtecke.clear();
		for(int i = 0; i < anzahl; i++) {
			rechtecke.add(Rechteck.generiereZufallsRechteck());
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void rechteckHinzufügen() {
		// TODO Auto-generated method stub
		new Rechteckgenerator(this);
	}

}
