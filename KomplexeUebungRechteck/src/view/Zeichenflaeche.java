package view;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")
public class Zeichenflaeche extends JPanel {

	private final BunteRechteckeController controlle; // final weil es ja nicht mehr ver�ndert werden soll

	public Zeichenflaeche(BunteRechteckeController controlle) {
		super();
		this.controlle = controlle;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.magenta);
		// g.drawRect(0, 0, 50, 50);
		for (int i = 0; this.controlle.getRechtecke().size() > i; i++) {
			Rechteck temp = this.controlle.getRechtecke().get(i);
			g.drawRect(temp.getX(), temp.getY(), temp.getBreite(), temp.getHoehe());
		}
	}

}
