package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

public class BunteRechteckGUI extends JFrame {

	private JPanel contentPane;
	private JMenuBar bar;
	private JMenu menu;
	private JMenuItem item1;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BunteRechteckGUI frame = new BunteRechteckGUI();
		frame.run();		
	}

	protected void run() {
		// TODO Auto-generated method stub
		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.revalidate();
			this.repaint();
		}
	}

	/**
	 * Create the frame.
	 */
	public BunteRechteckGUI() {
		this.brc = new BunteRechteckeController();
		//this.brc.add(new Rechteck(100,100,100,50));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1980, 1024);
		contentPane = new Zeichenflaeche(this.brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		this.bar = new JMenuBar();
		setJMenuBar(this.bar);
		this.menu = new JMenu("Hinzufügen");
		this.bar.add(this.menu);
		this.item1 = new JMenuItem("Rechteck hinzufügen");
		this.menu.add(this.item1);
		this.item1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				item1_Clicked();
				
			}});
		setVisible(true);
	}

	protected void item1_Clicked() {
		// TODO Auto-generated method stub
		this.brc.rechteckHinzufügen();
	}

}
