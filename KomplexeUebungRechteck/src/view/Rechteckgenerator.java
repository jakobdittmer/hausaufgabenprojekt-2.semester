package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.JButton;
import java.awt.GridLayout;
import java.util.LinkedList;

import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Rechteckgenerator extends JFrame {

	private JPanel contentPane;
	private JTextField b;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblBreite;
	private JLabel lblHhe;
	private JTextField y;
	private JTextField x;
	private JTextField h;
	private BunteRechteckeController ctr;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Rechteckgenerator frame = new Rechteckgenerator(new BunteRechteckeController());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param bunteRechteckeController 
	 */
	public Rechteckgenerator(BunteRechteckeController bunteRechteckeController) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		this.ctr = bunteRechteckeController;
		
		JButton btnNewButton = new JButton("Generiere");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ctr.add(new Rechteck(Integer.parseInt(x.getText()),
									Integer.parseInt(y.getText()),
									Integer.parseInt(b.getText()),
									Integer.parseInt(h.getText())));
			}
		});
		contentPane.add(btnNewButton, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(2, 4, 0, 0));
		
		lblX = new JLabel("X");
		panel.add(lblX);
		
		lblY = new JLabel("Y");
		panel.add(lblY);
		
		lblHhe = new JLabel("H\u00F6he");
		panel.add(lblHhe);
		
		lblBreite = new JLabel("Breite");
		panel.add(lblBreite);
		
		x = new JTextField();
		panel.add(x);
		x.setColumns(10);
		
		y = new JTextField();
		panel.add(y);
		y.setColumns(10);
		
		h = new JTextField();
		panel.add(h);
		h.setColumns(10);
		
		b = new JTextField();
		panel.add(b);
		b.setColumns(10);
		setVisible(true);
	}

}
