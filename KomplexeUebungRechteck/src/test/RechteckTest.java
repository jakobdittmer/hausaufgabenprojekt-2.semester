package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck rechteck0 = new Rechteck(10,10,30,40);
		Rechteck rechteck1 = new Rechteck(25,25,100,20);
		Rechteck rechteck2 = new Rechteck(260,10,200,100);
		Rechteck rechteck3 = new Rechteck(5,500,300,25);
		Rechteck rechteck4 = new Rechteck(100,100,100,100);
		Rechteck rechteck5 = new Rechteck();
		Rechteck rechteck6 = new Rechteck();
		Rechteck rechteck7 = new Rechteck();
		Rechteck rechteck8 = new Rechteck();
		Rechteck rechteck9 = new Rechteck();
		
		/*
		 * Sie wollen das ich 5 parameterlos erstelle mit vorgegebenen Attributen...hei�t ich soll 20 mal setter aufrufen???
		 * Sind sie nen Sadist? .__.
		 */

		rechteck5.setBreite(200);
		rechteck5.setHoehe(200);
		rechteck5.setX(200);
		rechteck5.setY(200);
		
		rechteck6.setBreite(20);
		rechteck6.setHoehe(20);
		rechteck6.setX(800);
		rechteck6.setY(400);
		
		rechteck7.setBreite(20);
		rechteck7.setHoehe(20);
		rechteck7.setX(800);
		rechteck7.setY(450);
		
		rechteck8.setBreite(20);
		rechteck8.setHoehe(20);
		rechteck8.setX(850);
		rechteck8.setY(400);
		
		rechteck9.setBreite(25);
		rechteck9.setHoehe(25);
		rechteck9.setX(855);
		rechteck9.setY(455);
		
		if(rechteck0.toString().equals("Rechteck: [x=10, y=10, breite=30, hoehe=40]")) {
			System.out.println("Baum");      // ^
		}
		
		/*
		 * was f�r ne verschwendung an Platz...
		 */
		
		BunteRechteckeController c1 = new BunteRechteckeController();
		c1.add(rechteck0);
		c1.add(rechteck1);
		c1.add(rechteck2);
		c1.add(rechteck3);
		c1.add(rechteck4);
		c1.add(rechteck5);
		c1.add(rechteck6);
		c1.add(rechteck7);
		c1.add(rechteck8);
		c1.add(rechteck9);
		System.out.println(c1.toString());
	
		
		//Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glichist
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
		System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50,hoehe=200]
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200,hoehe=100]
	
		System.out.println(Rechteck.generiereZufallsRechteck().toString()); //seems to work
		
		System.out.println(String.valueOf(RechteckTest.rechteckeTesten()));; //seems to work
	}
	
	public static boolean rechteckeTesten() { // Ich dachte, ich lasse mir mal nen boolean ausgeben
		Rechteck[] testreihe = new Rechteck[50000];
		Rechteck grenze = new Rechteck(0,0,1200,1000); //obwhol eig das Fenster 1260x1080 gro� ist aber ok, wenn sie das in der Email so sagen ._.
		for(int i = 0; i < 50000; i++) {
			testreihe[i] = Rechteck.generiereZufallsRechteck();
		}
		//System.out.println("GENERIERUNG DONE"); <--- Just to see if that above works
		for(int i = 0; i < 50000;i++) {			   //Man h�tte auch gleich jedes Rechteck, nach dem es generiert wurde, testen k�nnen
			if(grenze.enthaelt(testreihe[i]) !=true) {
				return false;
			}
		}
		return true;
	}

}
