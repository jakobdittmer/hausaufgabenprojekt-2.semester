package model;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class MySQLDatabase {
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/Rechtecke?";
	private String user = "root";
	private String password = "";
	
	public void rechteckEintragen(Rechteck r) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Rechtecke (x,y,breite, hoehe) VALUES\r\n" + 
						"(?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public LinkedList<Rechteck> getAlleRechtecke(){
		LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Rechtecke;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				int x = rs.getInt("x");
				int y =	rs.getInt("y");
				int breite = rs.getInt("breite");
				int hohe = rs.getInt("hohe");
				rechtecke.add(new Rechteck(x,y,breite,hohe));
			}
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
}
