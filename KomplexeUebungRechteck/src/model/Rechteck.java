package model;

/**
 * @author Jakob Dittmer
 * @version 2.0
 */

public class Rechteck {
	// Attribute
	private int x;
	private int y;
	private int breite;
	private int hoehe;

	// Construktor
	public Rechteck() {
		this.x = 0;
		this.y = 0;
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		this.x = Math.abs(x);
		this.y = Math.abs(y);
		this.breite = Math.abs(breite);
		this.hoehe = Math.abs(hoehe);
	}

	// Methoden
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = Math.abs(x);
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = Math.abs(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}

	@Override	//‹berschreibt die Tolle toString Methode welche von jedem Objekt geerbt wird.
	public String toString() {
		return "Rechteck: [x=" + this.x + ", y=" + this.y + ", breite=" + this.breite + ", hoehe=" + this.hoehe + "]"; //Keine wirkliche Verbesserung
	}
	
	public boolean enthaelt(int x, int y) {	
		if(this.y <= y && this.x <= x) {
			if(this.y + this.hoehe >= y && this.x + this.breite >= x) {
				return true;
			}
		}
		return false;
		/*	Oder so hier:
		 *  die x-Koordinate kontrollieren
		 *	if(this.x > x || this.x + this.breite > x)return false;
		 *	die y-Koordinate kontrollieren
		 *	if(this.y > y || this.y + this.hoehe > y)return false;
		 *	return ture;
		 */
	}
	
	public boolean enthaelt(Punkt p) {
		if (enthaelt(p.getX(),p.getY())) {
			return true;
		}
		return false;
	}

	 public boolean enthaelt(Rechteck rechteck) {
		return rechteck.enthaelt(this.x, this.y);
	 }
	
	 public static Rechteck generiereZufallsRechteck() {
		int h = (int) (Math.random()*1080);
		int b = (int) (Math.random()*1260);
		int y = (int) (Math.random()*(1080-h));
		int x = (int) (Math.random()*(1260-b));
		return new Rechteck(x,y,b,h);
	 }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
